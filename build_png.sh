#!/bin/bash
mkdir -p build/animator.iconset

cp animator_orig.png build/animator.iconset/icon_512x512@2x.png

convert animator_orig.png -resize 512x512 build/animator.iconset/icon_512x512.png
convert animator_orig.png -resize 256x256 build/animator.iconset/icon_256x256.png
convert animator_orig.png -resize 128x128 build/animator.iconset/icon_128x128.png
convert animator_orig.png -resize 64x64   build/animator.iconset/icon_32x32@2x.png
convert animator_orig.png -resize 32x32   build/animator.iconset/icon_32x32.png
convert animator_orig.png -resize 16x16   build/animator.iconset/icon_16x16.png

cp animator_orig.png build/animator.iconset/icon_512x512@2x.png
cp build/animator.iconset/icon_512x512.png build/animator.iconset/icon_256x256@2x.png
cp build/animator.iconset/icon_256x256.png build/animator.iconset/icon_128x128@2x.png
cp build/animator.iconset/icon_32x32.png build/animator.iconset/icon_16x16@2x.png
cp build/animator.iconset/icon_256x256.png animator.png
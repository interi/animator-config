# ANIMATOR config

The ANIMATOR configuration application. TODO: More here later

## Requirements

* Node.js (NPM)
* Native system compiler
	* Windows: Visual Studio (Express)
	* OSX: Xcode (homebrew is a good idea)
	* Linux: gcc (`sudo apt-get install build-essential` or equivalent should suffice)
* ** INTERNET ACCESS ** - Packages are downloaded / updated on the fly

## Setup

I've created an environment script to make things easier:

	source activate.sh
	
On OSX/Linux, it'll update the command line with the prefix "(node)". To exit, just exit the shell:

	exit

You only need to install the top level NPM packages once, by running:

	npm install
	
Doing it multiple times won't hurt anything, its just slow.

## Testing

The build system is robust and modular, so you can do a "quick build" for in-place testing to check the current state of the app:

	source activate.sh (if not already)
	gulp run
	
## Building

Once you're happy with the state of the application, you can run the build step to package the application. ** WARNING: Due to the complexity of the application, it can only be built for your host environment. This means this has to be done per-platform when preparing a release. **

	source activate.sh (if not already)
	gulp (or gulp build if you want to be verbose)
	
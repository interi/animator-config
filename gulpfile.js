var gulp = require('gulp');
var gutl = require('gulp-util');
var conf = require('./package');
var appf = require('./app/package');

var os = require('os');
var platform = (function () {
    switch(os.platform()) {
        case "darwin":
        case "win32":
            return os.platform();
        default:
            return os.platform() + "-" + os.arch();
    }
})();
conf['platform'] = platform;
appf['platform'] = platform;
appf['icon'] = conf['node-webkit'][platform]['icon'];

var fs = require('fs');
var path = require('path');
var spawn = require('child_process').spawn;

var _ = require('lodash');
var rimraf = require('rimraf');
var unzip = require('gulp-unzip');
var gulpif = require('gulp-if');
var rename = require('gulp-rename');
var lazypipe = require('lazypipe');
var through2 = require('through2');
var download = require('gulp-download');
var shell = require('gulp-shell');
var template = require('gulp-template');
var filter = require('gulp-filter');
var zip = require('gulp-zip');
var async = require('async');
var touch = require('touch');

var base_dir = path.join(conf['build-dir'], 'base');
var dist_dir = path.join(conf['build-dir'], 'dist');
var platform_app = path.join(base_dir, _.template(conf['node-webkit'][platform]['app'], appf));

gulp.task('download', function () {
    var out = through2.obj();
    var url = conf['node-webkit'][platform]['url'];
    var name = url.split('/').pop();
    fs.exists(path.join(conf['build-dir'], name), function (exists) {
        if(!exists) {
            var dl = download(url);
            dl.on('data', function (data) { out.push(data); });
            dl.on('end', function () { out.push(null); });
        } else {
            out.push(null);
        }
    });
    return out.pipe(gulp.dest(conf['build-dir']));
});

var tar_workaround = function (base) {
    return through2.obj(function (chunk, enc, callback) {
        var stream = this;
        var md     = spawn('mkdir', ['-p', '' + base]);
        md.on('close', function (code) {
            if(code != 0) return stream.emit('error', new gutl.PluginError('tar_workaround', 'Unable to create extraction directory'));
            var tar    = spawn('tar', ['xvf', chunk.path, '-C', base]);
            var root   = null;
            var files  = [];
            tar.stdout.on('data', function (data) {
                _.each(data.toString().trim().split('\n'), function (item) {
                    if(!root) {
                        root = item;
                        return;
                    }
                    gutl.log('Extracted: ', item);
                    files.push(item);
                });
            });
            tar.on('close', function (code) {
                if(code != 0) return stream.emit('error', new gutl.PluginError('tar_workaround', 'Tar extraction error: ' + code));
                async.each(files, function (item, cb) {
                    fs.readFile(path.join(base, item), function (err, data) {
                        if(err) return cb(err);
                        stream.push(new gutl.File({
                            path: item,
                            contents: data
                        }));
                        cb();
                    });
                }, function (err) {
                    rimraf(path.join(base, root), function () {
                        stream.push(null);
                    });
                });
            });
        });
    });
};

var strip_prefix = function (p) {
    var parts = p.dirname.split(path.sep);
    if(parts.length > 0) {
        parts.shift();
        p.dirname = path.join.apply(path.join, parts);
    }
}

var extract_tar = lazypipe()
    .pipe(tar_workaround, conf['build-dir'])
    .pipe(rename, strip_prefix);

var extract_zip = lazypipe()
    .pipe(unzip)
    .pipe(rename, strip_prefix);

gulp.task('extract', ['download'], function () {
    var url = conf['node-webkit'][platform]['url'];
    var name = url.split('/').pop();
    var trgt = path.join(conf['build-dir'], 'base');
    var out = through2.obj();
    fs.exists(trgt, function (exists) {
        if(!exists) {
            var ext = gulp.src(path.join(conf['build-dir'], name))
                .pipe(gulpif("*.zip", extract_zip()))
                .pipe(gulpif("*.tar.gz", extract_tar()))
                .pipe(gulp.dest(trgt));
            ext.on('data', function (data) { out.push(data); });
            ext.on('end' , function ()     { out.push(null); });
        } else {
            out.push(null);
        }
    });
    return out;
});

var build_native = function (name) {
    return function() {
        var out = through2.obj();
        var pid = path.join(conf['build-dir'], name + '.nw-build');
        fs.exists(pid, function (exists) {
            if(!exists) {
                var dir = path.join(conf['app-dir'], 'node_modules', name);
                var cmd = 'node-pre-gyp build --runtime=node-webkit --target=' + conf['node-webkit'][platform]['version'];
                var run = gulp.src("")
                    .pipe(shell([cmd], { cwd: dir }));
                run.on('data', function (data) { out.push(data); });
                run.on('end' , function ()     {
                    touch(pid, {}, function () {
                        out.push(null);
                    });
                });
            } else {
                out.push(null);
            }
        });
        return out;
    }
}

gulp.task('npm', shell.task(['npm install'], { cwd: conf['app-dir'] }));

var install_deps = ['npm'];
_.each(appf['native'], function (item) {
    var name = 'build-' + item;
    gulp.task(name, ['npm'], build_native(item));
    install_deps.push(name);
});

gulp.task('install', install_deps);

gulp.task('darwin-rename', ['extract'], function () {
    var out = through2.obj();
    var base = path.join(base_dir, 'node-webkit.app');
    var dest = path.join(base_dir, appf['window']['title'] + ".app");
    fs.exists(dest, function (exists) {
        if(!exists) {
            var copy = gulp.src(path.join(base, '**'))
                .pipe(gulp.dest(dest));
            copy.on('data', function (data) { out.push(data); });
            copy.on('end' , function ()     { out.push(null); });
        } else {
            out.push(null);
        }
    });
    return out;
});
gulp.task('darwin-plist', ['extract', 'darwin-rename'], function () {
    return gulp.src('Info.plist')
        .pipe(template(appf))
        .pipe(gulp.dest(path.join(path.dirname(platform_app), '..')));
});
gulp.task('darwin-icon', ['extract', 'darwin-rename'], function () {
    return gulp.src(appf['icon'])
        .pipe(gulp.dest(path.join(path.dirname(platform_app), '..', 'Resources')));
});
gulp.task('darwin-prebuild', ['darwin-plist', 'darwin-icon']);

gulp.task('linux-icon', ['extract'], function () {
    return gulp.src(['gen_link.sh', 'link.tmpl'])
        .pipe(template(appf))
        .pipe(gulp.dest(base_dir));
});
gulp.task('linux-prebuild', ['extract', 'linux-icon'], function () {
    return gulp.src(appf['icon'])
        .pipe(gulp.dest(base_dir));
});

gulp.task('win32-prebuild', ['extract'], function () {
    var cmd = 'reshack.exe -addoverwrite "' + platform_app + '","' + platform_app + '",' + 
    '"<%= file.path %>",icongroup,IDR_MAINFRAME,';
    return gulp.src(appf['icon'])
        .pipe(shell([cmd]));
});

gulp.task('prebuild', ['install', os.platform() + "-prebuild"]);

gulp.task('run', ['prebuild'], shell.task([
    '"' + platform_app + '" ' + conf['app-dir']
]));

gulp.task('clean', function (cb) {
    rimraf(dist_dir, cb);
});

gulp.task('package', ['prebuild', 'clean'], function () {
    gulp.src(path.join(conf['app-dir'], '**'))
        .pipe(zip('app.nw'))
        .pipe(gulp.dest(dist_dir));
});

gulp.task('copy', ['prebuild', 'clean'], function () {
    return gulp.src([
        path.join(base_dir, '**'),
        '!' + platform_app,
        '!' + path.join(base_dir, 'nwsnapshot')
    ])
        .pipe(gulp.dest(dist_dir));
});

gulp.task('darwin-copy', ['darwin-prebuild', 'clean'], function () {
    return gulp.src(path.join(base_dir, appf['window']['title'] + '.app', '**'))
        .pipe(gulp.dest(path.join(dist_dir, appf['window']['title'] + '.app')));
});

gulp.task('darwin-app', ['darwin-copy', 'package'], function () {
    var src  = path.join(conf['app-dir'], '**');
    var dest = path.join(dist_dir, appf['window']['title'] + '.app', 'Contents', 'Resources', 'app.nw');
    return gulp.src('app/**')
        .pipe(gulp.dest(dest));
});

gulp.task('linux-app', ['copy', 'package'], function () {
    var app = path.join(dist_dir, appf['name']);
    var cmd = 'cat "' + platform_app + '" "' + path.join(dist_dir, 'app.nw') + '" > "' 
        + app + '" && chmod +x "' + app + '"';
    return gulp.src(platform_app)
        .pipe(shell([cmd]));
});

gulp.task('win32-app', ['copy', 'package'], function () {
    var app = path.join(dist_dir, 'nw.exe');
    var cmd = 'copy /b "' + platform_app + '"+"' + path.join(dist_dir, 'app.nw') + '" "' 
        + app + '"';
    return gulp.src(platform_app)
        .pipe(shell([cmd]));
});

gulp.task('build', [os.platform() + '-app'], function () {
    return gulp.src([
        path.join(dist_dir, '**'),
        '!' + path.join(dist_dir, '*.zip'),
        '!' + path.join(dist_dir, '*.nw'),
        '!' + path.join(dist_dir, '*.desktop')
    ])
        .pipe(zip(appf['name'] + '-' + platform + '-' + appf['version'] + '.zip'))
        .pipe(gulp.dest(dist_dir));
});

gulp.task('default', ['build'])